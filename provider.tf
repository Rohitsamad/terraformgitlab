provider "aws" {
  version = "~> 3.0"
  region     = "us-east-2"
}

resource "aws_instance" "myec2" {
   ami = "ami-074cce78125f09d61"
   instance_type = "t2.micro"
}
